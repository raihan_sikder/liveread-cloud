<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUpoloadsElementIdUuid extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::statement("ALTER TABLE `spyr42_uploads` CHANGE `element_uuid` `element_uuid` VARCHAR(36) NULL DEFAULT NULL;");
        DB::statement("ALTER TABLE `spyr42_uploads` CHANGE `element_id` `element_id` INT UNSIGNED NULL DEFAULT NULL;");

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
