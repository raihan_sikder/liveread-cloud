<?php
/**
This form.blade.php view is rendered by SpyrmodulebaseControllers edit() and update() methods.
Based on whether it is being called by edit() or update() you will get access to some already
instantiated variables that might be helpful. You can directly check what variables are passed
to view using  "->with(var,value)" in the Controller.

Let's consider that the module name is 'superheroes' where a list of super heros are managed

// ------------------------------------------------------------------------------------------
// default variables. This are always available for both updating a creating
// ------------------------------------------------------------------------------------------
$module_name = 'superheroes' // String, plural
$mod = (the row from modules table as Module object by Module::whereName($this->module_name)->first())
$spyr_element_editable = Value of this element is set based on logic/permission scheme.

// ------------------------------------------------------------------------------------------
// updating
// ------------------------------------------------------------------------------------------
$element = 'superhero' // String, singular, this is the element that is being updated
$$element = Object that is being edited
$superhero =  Value same as above. Variable name with better readability.
$spyr_element_editable = is set true or false based on the editibilty logic - isEditable()

// ------------------------------------------------------------------------------------------
// creating
// ------------------------------------------------------------------------------------------
$uuid = Generated uuid for the element that is being created
$spyr_element_editable = set as 'TRUE' by default
 */
/**
 * Variables used in this view file.
 * @var $module_name string 'superheroes'
 * @var $mod Module
 * @var $superhero Superhero Object that is being edited
 * @var $element string 'superhero'
 * @var $spyr_element_editable boolean
 * @var $uuid string '1709c091-8114-4ba4-8fd8-91f0ba0b63e8'
 */
?>


{{-- Form starts: Form fields are placed here. These will be added inside the spyrframe default form container in
 app/views/spyr/modules/base/form.blade.php --}}
@include('spyr.form.input-text',['var'=>['name'=>'name','label'=>'Name', 'container_class'=>'col-sm-6']])
@include('spyr.form.is_active')
{{-- Form ends --}}

{{-- JS starts: javascript codes go here.--}}
@section('js')
    @parent
    <script type="text/javascript">
        @if(!isset($$element->id))
        /*******************************************************************/
        // Creating :
        // this is a place holder to write  the javascript codes
        // during creation of an element. As this stage $$element or $superhero(module
        // name singular) is not set, also there is no id is created
        // Following the convention of spyrframe you are only allowed to call functions
        /*******************************************************************/

        // your functions go here
        // function1();
        // function2();
        @else
        /*******************************************************************/
        // Updating :
        // this is a place holder to write  the javascript codes that will run
        // while updating an element that has been already created.
        // during update the variable $$element or $superhero(module
        // name singular) is set, and id like other attributes of the element can be
        // accessed by calling $$element->id, also $superhero->id
        // Following the convention of spyrframe you are only allowed to call functions
        /*******************************************************************/

        // your functions go here
        // function1();
        // function2();
        @endif

        /*******************************************************************/
        // Saving :
        // Saving covers both creating and updating (Similar to Eloquent model event)
        // However, it is not guaranteed $$element is set. So the code here should
        // be functional for both case where an element is being created or already
        // created. This is a good place for writing validation
        // Following the convention of spyrframe you are only allowed to call functions
        /*******************************************************************/

        // your functions goe here
        // function1();
        // function2();

        /*******************************************************************/
        // frontend and Ajax hybrid validation
        /*******************************************************************/
        addValidationRulesForSaving(); // Assign validation classes/rules
        enableValidation('{{$module_name}}'); // Instantiate validation function

        /*******************************************************************/
        // List of functions
        /*******************************************************************/

        // Assigns validation rules during saving (both creating and updating)
        function addValidationRulesForSaving() {
            $("input[name=name]").addClass('validate[required]');
        }
    </script>
@stop
{{-- JS ends --}}