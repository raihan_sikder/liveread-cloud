<?php

/*
|--------------------------------------------------------------------------
| Spyr Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the spyr framework.
|
*/

Route::group(['prefix' => 'test'], function () {
    Route::get('add-user-to-group', function () {
        $group = Sentry::getGroupProvider()->findById(conf('var.tenant-admin.group-id'));
        $user = User::find(35);

        $user->addGroup($group);

        // dd();
    });
    Route::get('save-all-uploads', function () {
        $uploads = Upload::all();
        foreach ($uploads as $upload) {

            $upload->save();
            echoBr($upload->id);
            echoBr($upload->path);
            echoBr(extFrmPath($upload->path));
            echoBr($upload->ext);

        }
    });

    Route::get('tenant-models', function () {
        User()->testFunction();
        dd();
    });

});
