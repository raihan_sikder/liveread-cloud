<?php

class DashboardController extends BaseController
{

    /*
    |--------------------------------------------------------------------------
    | Default dashboard controller
    |--------------------------------------------------------------------------
    |
    | This controller generates dashboards for different type of users related functionalities
    |
    */

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show()
    {
        if (!Sentry::check()) {
            return Redirect::route('get.signin');
        }
        return View::make('spyr.dashboards.default');
    }


}
