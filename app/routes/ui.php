<?php

/*
|--------------------------------------------------------------------------
| Spyr Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the spyr framework.
|
*/
Route::group(array('prefix' => 'ux'), function () {
    Route::get('app-frame', function () {
        return View::make('spyr.template.app-frame');
    });
    Route::get('auth-frame', function () {
        return View::make('spyr.template.auth-frame');
    });
    Route::get('index', function () {
        return View::make('spyr._template.index');
    });
});
