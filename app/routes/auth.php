<?php

/*
|--------------------------------------------------------------------------
| Authentication and Registration Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for authentication.
|
*/
Route::group(['prefix' => 'auth'], function () {

    # Register
    Route::get('signup', ['as' => 'get.signup', 'uses' => 'AuthController@getSignup']);
    Route::post('signup', ['as' => 'post.signup', 'uses' => 'AuthController@postSignup']);

    # Todo : Write you social login urls here

    # Account Activation
    Route::get('activate/{activationCode}', ['as' => 'activate', 'uses' => 'AuthController@getActivate']);

    # Email Activation
    Route::get('reset-email/{activationCode}', ['as' => 'reset-email', 'uses' => 'ChangeEmailController@getActivateResetEmail']);

    # Login
    Route::get('signin', ['as' => 'get.signin', 'uses' => 'AuthController@getSignin']);
    Route::post('signin', ['as' => 'post.signin', 'uses' => 'AuthController@postSignin']);

    # Forgot Password
    Route::get('forgot-password', ['as' => 'forgot-password', 'uses' => 'AuthController@getForgotPassword']);
    Route::post('forgot-password', 'AuthController@postForgotPassword');

    # Forgot Password Confirmation
    Route::get('forgot-password/{passwordResetCode}', ['as' => 'forgot-password-confirm', 'uses' => 'AuthController@getForgotPasswordConfirm']);
    Route::post('forgot-password/{passwordResetCode}', 'AuthController@postForgotPasswordConfirm');

    # Logout
    Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@getLogout']);

});
