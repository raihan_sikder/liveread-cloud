<?php

/**
 * Module
 *
 * @property integer $id
 * @property string $uuid
 * @property string $name
 * @property string $title
 * @property string $desc
 * @property integer $parent_id
 * @property integer $modulegroup_id
 * @property integer $level
 * @property integer $order
 * @property string $color_css
 * @property string $icon_css
 * @property string $route
 * @property string $has_uploads
 * @property string $has_messages
 * @property string $is_active
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property integer $deleted_by
 * @property-read \User $updater
 * @property-read \User $creator
 * @method static \Illuminate\Database\Query\Builder|\Module whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereDesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereModulegroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereLevel($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereColorCss($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereIconCss($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereRoute($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereHasUploads($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereHasMessages($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereDeletedBy($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Upload[] $uploads
 * @property-read \Illuminate\Database\Eloquent\Collection|\Revision[] $revisions
 */
class Module extends Spyrmodule
{
    /**
     * Custom validation messages.
     * @var array
     */
    public static $custom_validation_messages = [
        //'name.required' => 'Custom message.',
    ];

    /**
     * Disallow from mass assignment. (Black-listed fields)
     * @var array
     */
    // protected $guarded = [];

    /**
     * Date fields
     * @var array
     */
    // protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    /**
     * Mass assignment fields (White-listed fields)
     * @var array
     */
    protected $fillable = ['name', 'title', 'desc', 'parent_id', 'modulegroup_id', 'level', 'order', 'color_css', 'icon_css', 'route', 'has_uploads', 'has_messages', 'is_active', 'created_by', 'updated_by', 'deleted_by'];

    /**
     * Validation rules. For regular expression validation use array instead of pipe
     * Example: 'name' => ['required', 'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/']
     * @param       $element
     * @param array $merge
     * @return array
     */
    public static function rules($element, $merge = [])
    {
        $rules = [
            'name' => 'required|between:1,255|unique:modules,name' . (isset($element->id) ? ",$element->id" : ''),
            'title' => 'required|between:1,255|unique:modules,title' . (isset($element->id) ? ",$element->id" : ''),
            'created_by' => 'exists:users,id,is_active,"Yes"',
            'updated_by' => 'exists:users,id,is_active,"Yes"',
            'has_uploads' => 'in:Yes,No',
            'has_messages' => 'in:Yes,No',
            'is_active' => 'required|in:Yes,No',
        ];
        return array_merge($rules, $merge);
    }
    /**
     * Automatic eager load relation by default (can be expensive)
     * @var array
     */
    // protected $with = ['relation1', 'relation2'];

    ############################################################################################
    # Model events
    ############################################################################################

    public static function boot()
    {
        /**
         * parent::boot() was previously used. However this invocation stops from the other classes
         * of other spyr modules(Models) to override the boot() method. Need to check more.
         * make the parent (Eloquent) boot method run.
         */
        parent::boot();
        Spyrmodule::registerObserver(get_class()); // register observer

        /************************************************************/
        // Following code block executes - when an element is in process
        // of creation for the first time but the creation has not
        // completed yet.
        /************************************************************/
        // static::creating(function (Module $element) { });

        /************************************************************/
        // Following code block executes - after an element is created
        // for the first time.
        /************************************************************/
        // static::created(function (Module $element) {});

        /************************************************************/
        // Following code block executes - when an already existing
        // element is in process of being updated but the update is
        // not yet complete.
        /************************************************************/
        // static::updating(function (Module $element) {});

        /************************************************************/
        // Following code block executes - after an element
        // is successfully updated
        /************************************************************/
        //static::updated(function (Module $element) {});

        /************************************************************/
        // Execute codes during saving (both creating and updating)
        /************************************************************/
//        static::saving(function (Superhero $element) {
//            $valid = true;
//            /************************************************************/
//            // Your validation goes here
//            // if($valid) $valid = $element->isSomethingDoable(true)
//            /************************************************************/
//            return $valid;
//        });

        /************************************************************/
        // Execute codes after model is successfully saved
        /************************************************************/
        // static::saved(function (Superhero $element) {});

        /************************************************************/
        // Following code block executes - when some element is in
        // the process of being deleted. This is good place to
        // put validations for eligibility of deletion.
        /************************************************************/
        // static::deleting(function (Module $element) {});

        /************************************************************/
        // Following code block executes - after an element is
        // successfully deleted.
        /************************************************************/
        // static::deleted(function (Module $element) {});

        /************************************************************/
        // Following code block executes - when an already deleted element
        // is in the process of being restored.
        /************************************************************/
        // static::restoring(function (Module $element) {});

        /************************************************************/
        // Following code block executes - after an element is
        // successfully restored.
        /************************************************************/
        //static::restored(function (Module $element) {});
    }

    ############################################################################################
    # Validator functions
    ############################################################################################

    /**
     * @param bool|false $setMsgSession setting it false will not store the message in session
     * @return bool
     */
    //    public function isSomethingDoable($setMsgSession = false)
    //    {
    //        $valid = true;
    //        // Make invalid(Not request-able) if {something doesn't match with something}
    //        if ($valid && $this->id == null) {
    //            if ($setMsgSession) $valid = setError("Something is wrong. Id is Null!!"); // make valid flag false and set validation error message in session if message flag is true
    //            else $valid = false; // don't set any message only set validation as false.
    //        }
    //
    //        return $valid;
    //    }

    ############################################################################################
    # Helper functions
    ############################################################################################
    /**
     * Non static functions can be directly called $element->function();
     * Such functions are useful when an object(element) is already instantiated
     * and some processing is required for that
     */
    // public function someAction() { }

    /**
     * Static cuntions needs to be called using Model::function($id)
     * Inside static function you may need to query and get the element
     * @param $id
     */
    // public static function someOtherAction($id) { }

    /**
     * Get module names as one-dimentional array, by default get only the active ones
     * @param bool|true $only_active
     * @return array
     */
    public static function names($only_active = true)
    {
        $q = Module::select('name');
        if ($only_active) {
            $q = $q->where('is_active', 'Yes');
        }
        $results = $q->remember(cacheTime('module-list'))->get()->toArray();
        $modules = array_column($results, 'name');
        return $modules;
    }

    /**
     * Checks if a module is a module with tenant context
     * @param $module_name
     * @return bool|mixed
     */
    public static function hasTenantContext($module_name)
    {
        $tenant_idf = tenantIdField();
        $fields = columns($module_name);

        if (in_array($tenant_idf, $fields)) { // tenant id field found
            return true;
        }
        return false;
    }

    /**
     * Function returns an array of predecessor module objects of a specific module.
     * This function is helpful to generate breadcrumb or menu.
     * @return array
     */
    public function moduletree()
    {

        $stack = [$this];
        for ($i = $this->parent_id; ;) {
            if (!$i) break;

            if ($predecessor = Module::find($i)) {
                array_push($stack, $predecessor);
                $i = $predecessor->parent_id;
            }
        }

        $stack = array_reverse($stack);

        return $stack;
    }

    /**
     * Returns a multi dimentional array with hiararchically stored modulegroups and modules.
     * Unlike previous moduletree() function, this one check the parent relationship with
     * modulegroup instead of module.
     * @return array
     */
    public function modulegroupTree()
    {
        $stack = [$this];
        for ($i = $this->modulegroup_id; ;) {
            if (!$i) break;
            if ($predecessor = Modulegroup::remember(60)->find($i)) {
                array_push($stack, $predecessor);
                $i = $predecessor->parent_id;
            }
        }
        $stack = array_reverse($stack);
        return $stack;
    }

    ############################################################################################
    # Permission functions
    # ---------------------------------------------------------------------------------------- #
    /*
     * This is a place holder to write the functions that resolve permission to a specific model.
     * In the parent class there are the follow functions that checks whether a user has
     * permission to perform the following on an element. Rewrite these functions
     * in case more customized access management is required.
     */
    ############################################################################################

    /**
     * Checks if the $module is viewable by current or any user passed as parameter.
     * spyrElementViewable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     * @param null $user_id
     * @return bool
     */
    //    public function isViewable($user_id = null)
    //    {
    //        $valid = false;
    //        if ($valid = spyrElementViewable($this, $user_id)) {
    //            //if ($valid && somethingElse()) $valid = false;
    //        }
    //        return $valid;
    //    }

    /**
     * Checks if the $module is editable by current or any user passed as parameter.
     * spyrElementEditable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     * @param null $user_id
     * @return bool
     */
    //    public function isEditable($user_id = null)
    //    {
    //        $valid = false;
    //        if ($valid = spyrElementEditable($this, $user_id)) {
    //            //if ($valid && somethingElse()) $valid = false;
    //        }
    //        return $valid;
    //    }

    /**
     * Checks if the $module is deletable by current or any user passed as parameter.
     * spyrElementDeletable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     * @param null $user_id
     * @return bool
     */
    //    public function isDeletable($user_id = null)
    //    {
    //        $valid = false;
    //        if ($valid = spyrElementDeletable($this, $user_id)) {
    //            //if ($valid && somethingElse()) $valid = false;
    //        }
    //        return $valid;
    //    }

    /**
     * Checks if the $module is restorable by current or any user passed as parameter.
     * spyrElementRestorable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     * @param null $user_id
     * @return bool
     */
    //    public function isRestorable($user_id = null)
    //    {
    //        $valid = false;
    //        if ($valid = spyrElementRestorable($this, $user_id)) {
    //            //if ($valid && somethingElse()) $valid = false;
    //        }
    //        return $valid;
    //    }

    ############################################################################################
    # Query scopes
    # ---------------------------------------------------------------------------------------- #
    /*
     * Scopes allow you to easily re-use query logic in your models. To define a scope, simply
     * prefix a model method with scope:
     */
    /*
       public function scopePopular($query) { return $query->where('votes', '>', 100); }
       public function scopeWomen($query) { return $query->whereGender('W'); }
       # Example of user
       $users = User::popular()->women()->orderBy('created_at')->get();
    */
    ############################################################################################

    // Write new query scopes here.

    ############################################################################################
    # Dynamic scopes
    # ---------------------------------------------------------------------------------------- #
    /*
     * Scopes allow you to easily re-use query logic in your models. To define a scope, simply
     * prefix a model method with scope:
     */
    /*
    public function scopeOfType($query, $type) { return $query->whereType($type); }
    # Example of user
    $users = User::ofType('member')->get();
    */
    ############################################################################################

    // Write new dynamic query scopes here.

    ############################################################################################
    # Model relationships
    # ---------------------------------------------------------------------------------------- #
    /*
     * This is a place holder to write model relationships. In the parent class there are
     * In the parent class there are the follow two relations creator(), updater() are
     * already defined.
     */
    ############################################################################################

    # Default relationships already available in base Class 'Spyrmodule'
    //public function updater() { return $this->belongsTo('User', 'updated_by'); }
    //public function creator() { return $this->belongsTo('User', 'created_by'); }

    // Write new relationships below this line

    ############################################################################################
    # Accessors & Mutators
    # ---------------------------------------------------------------------------------------- #
    /*
     * Eloquent provides a convenient way to transform your model attributes when getting or setting them. Simply
     * define a getFooAttribute method on your model to declare an accessor. Keep in mind that the methods
     * should follow camel-casing, even though your database columns are snake-case:
     */
    // Example
    // public function getFirstNameAttribute($value) { return ucfirst($value); }
    // public function setFirstNameAttribute($value) { $this->attributes['first_name'] = strtolower($value); }
    ############################################################################################

    // Write accessors and mutators here.

}
