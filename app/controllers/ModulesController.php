<?php


class ModulesController extends SpyrmodulebaseController
{

    public function __construct()
    {
        $this->module_name = controllerModule(get_class());

        parent::__construct($this->module_name);
    }
}
