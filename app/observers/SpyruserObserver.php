<?php

class SpyruserObserver extends ModuleBaseObserver
{

    /**
     * This function is executed during a model's saving() phase
     *
     * @param $element
     */
    public function saving($element)
    {
        $element = fillModel($element, $except = ['password']); // This line should be placed just before return
        Revision::keepChangesInSession($element); // store change log
    }
}