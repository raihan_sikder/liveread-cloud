<?php

/**
 * Spyrmodule
 * Base model for all spyrmodules.
 *
 * @property-read \User $updater
 * @property-read \User $creator
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property int $id
 * @property-read \Illuminate\Database\Eloquent\Collection|\Upload[] $uploads
 * @property-read \Illuminate\Database\Eloquent\Collection|\Revision[] $revisions
 */

class Spyrmodule extends Eloquent
{
    /**
     * Custom validation messages.
     * @var array
     */
    public static $custom_validation_messages = [
        //'name.required' => 'Custom message.',
    ];
    /**
     * Mass assignment fields (White-listed fields)
     * @var array
     */
    protected $fillable = ['uuid', 'name', 'tenant_id', 'is_active', 'created_by', 'updated_by', 'deleted_by'];
    /**
     * Disallow from mass assignment. (Black-listed fields)
     * @var array
     */
    protected $guarded = [];

    // Enable soft deleting.
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    /**
     * Date fields
     * @var array $dates
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Validation rules. For regular expression validation use array instead of pipe
     * Example: 'name' => ['required', 'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/']
     * @param       $element
     * @param array $merge
     * @return array
     */
    public static function rules($element, $merge = [])
    {
        $rules = [
            'name' => 'required|between:1,255|unique:spyrmodules,name' . (isset($element->id) ? ",$element->id" : ''),
            'created_by' => 'integer|exists:users,id,is_active,"Yes"',
            'updated_by' => 'integer|exists:users,id,is_active,"Yes"',
            'is_active' => 'required|in:Yes,No',
            'tenant_id' => 'required|tenants,id,is_active,"Yes"',
        ];

        return array_merge($rules, $merge);
    }
    /**
     * Automatic eager load relation by default (can be expensive)
     * @var array
     */
    // protected $with = ['relation1', 'relation2'];

    ############################################################################################
    # Model events
    ############################################################################################
    // Model events are handled in individual models that is extended from this base module.
    // So no model event instantiated in this class.

    public static function boot()
    {
        parent::boot(); // This is required to boot SoftDeletingTrait
    }

    ############################################################################################
    # Validator functions
    ############################################################################################
    /**
     * @param bool|false $setMsgSession setting it false will not store the message in session
     * @return bool
     */
    //    public function isSomethingDoable($setMsgSession = false)
    //    {
    //        $valid = true;
    //        // Make invalid(Not request-able) if {something doesn't match with something}
    //        if ($valid && $this->id == null) {
    //            if ($setMsgSession) $valid = setError("Something is wrong. Id is Null!!"); // make valid flag false and set validation error message in session if message flag is true
    //            else $valid = false; // don't set any message only set validation as false.
    //        }
    //
    //        return $valid;
    //    }

    ############################################################################################
    # Helper functions
    ############################################################################################
    /**
     * Non static functions can be directly called $element->function();
     * Such functions are useful when an object(element) is already instantiated
     * and some processing is required for that
     */
    // public function someAction() { }

    /**
     * Static cuntions needs to be called using Model::function($id)
     * Inside static function you may need to query and get the element
     * @internal param $id
     */
    // public static function someOtherAction($id) { }

    /**
     * Register the model observer for a spyr module
     * @param $Model
     */
    public static function registerObserver($Model)
    {
        $Model = "\\" . $Model;
        $ObserverClass = $Model . "Observer";
        /** @var  ModuleBaseObserver $Model */
        $Model::observe(new $ObserverClass); // register observer
    }

    ############################################################################################
    # Permission functions
    # ---------------------------------------------------------------------------------------- #
    /*
     * This is a place holder to write the functions that resolve permission to a specific model.
     * In the parent class there are the follow functions that checks whether a user has
     * permission to perform the following on an element. Rewrite these functions
     * in case more customized access management is required.
     */
    ############################################################################################

    /**
     * Checks if the $element is deletable by current or any user passed as parameter.
     * spyrElementDeletable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     * @param null $user_id
     * @param bool $set_msg
     * @return bool
     */
    public function isDeletable($user_id = null, $set_msg = false)
    {
        if ($valid = spyrElementDeletable($this, $user_id, $set_msg)) {
            if ($this->isEditable($user_id, $set_msg)) {
                // Write new validation logic for this operation in this block.
            }
        }
        return $valid;
    }

    /**
     * Checks if the $element is editable by current or any user passed as parameter.
     * spyrElementEditable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     * @param null $user_id
     * @param bool $set_msg
     * @return bool
     */
    public function isEditable($user_id = null, $set_msg = false)
    {
        if ($valid = spyrElementEditable($this, $user_id, $set_msg)) {
            if ($this->isViewable($user_id, $set_msg)) {
                // Write new validation logic for this operation in this block.
            }
        }
        return $valid;
    }

    /**
     * Checks if the $element is viewable by current or any user passed as parameter.
     * spyrElementViewable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     * @param null $user_id
     * @param bool $set_msg
     * @return bool
     */
    public function isViewable($user_id = null, $set_msg = false)
    {
        if ($valid = spyrElementViewable($this, $user_id, $set_msg)) {
            if ($this->isCreatable($user_id, $set_msg)) {
                // Write new validation logic for this operation in this block.
            }
        }
        return $valid;
    }

    /**
     * Checks if the $element is creatable by current or any user passed as parameter.
     * spyrElementViewable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     * @param null $user_id
     * @param bool $set_msg
     * @return bool
     */
    public function isCreatable($user_id = null, $set_msg = false)
    {
        if ($valid = spyrElementCreatable($this, $user_id, $set_msg)) {
            // Write new validation logic for this operation in this block.
        }
        return $valid;
    }

    /**
     * Checks if the $element is restorable by current or any user passed as parameter.
     * spyrElementRestorable() is the primary default checker based on permission
     * whether this should be allowed or not. The logic can be further
     * extend to implement more conditions.
     * @param null $user_id
     * @param bool $set_msg
     * @return bool
     */
    public function isRestorable($user_id = null, $set_msg = false)
    {
        if ($valid = spyrElementRestorable($this, $user_id, $set_msg)) {
            if ($this->isEditable($user_id, $set_msg)) {
                // Write new validation logic for this operation in this block.
            }
        }
        return $valid;
    }

    ############################################################################################
    # Query scopes
    # ---------------------------------------------------------------------------------------- #
    /*
     * Scopes allow you to easily re-use query logic in your models. To define a scope, simply
     * prefix a model method with scope:
     */
    /*
       public function scopePopular($query) { return $query->where('votes', '>', 100); }
       public function scopeWomen($query) { return $query->whereGender('W'); }
       # Example of user
       $users = User::popular()->women()->orderBy('created_at')->get();
    */
    ############################################################################################

    // Write new query scopes here.

    ############################################################################################
    # Dynamic scopes
    # ---------------------------------------------------------------------------------------- #
    /*
     * Scopes allow you to easily re-use query logic in your models. To define a scope, simply
     * prefix a model method with scope:
     */
    /*
    public function scopeOfType($query, $type) { return $query->whereType($type); }
    # Example of user
    $users = User::ofType('member')->get();
    */
    ############################################################################################

    // Write new dynamic query scopes here.

    ############################################################################################
    # Model relationships
    # ---------------------------------------------------------------------------------------- #
    /*
     * This is a place holder to write model relationships. In the parent class there are
     * In the parent class there are the follow two relations creator(), updater() are
     * already defined.
     */
    ############################################################################################
    /**
     * Get a list of uploads under an element.
     * @return mixed
     */
    public function uploads()
    {
        return $this->hasMany('Upload', 'element_id')->where('module_id', $this->module()->id);
    }

    /**
     * Get the module object that an element belongs to. If the element is $tenant then the function
     * returns the row from modules table that has module name 'tenants'.
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function module()
    {
        return Module::where('name', moduleName(get_class($this)))->remember(cacheTime('module-list'))->first();
    }

    /**
     * Get a list of revisions/changes that happened to an element
     * @return mixed
     */
    public function revisions()
    {
        return $this->hasMany('Revision', 'element_id')->where('module_id', $this->module()->id);
    }

    /**
     * Get the user who has created the element
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo('User', 'created_by');
    }

    /**
     * Get the user who has last updated the element
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updater()
    {
        return $this->belongsTo('User', 'updated_by');
    }

    ############################################################################################
    # Accessors & Mutators
    # ---------------------------------------------------------------------------------------- #
    /*
     * Eloquent provides a convenient way to transform your model attributes when getting or setting them. Simply
     * define a getFooAttribute method on your model to declare an accessor. Keep in mind that the methods
     * should follow camel-casing, even though your database columns are snake-case:
     */
    // Example
    // public function getFirstNameAttribute($value) { return ucfirst($value); }
    // public function setFirstNameAttribute($value) { $this->attributes['first_name'] = strtolower($value); }
    ############################################################################################

    // Write accessors and mutators here.
}
