<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Default System Cache timeout
    |--------------------------------------------------------------------------
    |
    | Supported: Any integer with value greater than zero
    |
    */
    'system-cache-time'       => 60,
    /*
	|--------------------------------------------------------------------------
	| File root
	|--------------------------------------------------------------------------
	|
	| Supported: Any integer with value greater than zero
	|
	*/
    'file-upload-root'        => '/files/',

    /*
	|--------------------------------------------------------------------------
	| Tenant field name in table
	|--------------------------------------------------------------------------
	|
	| Existance of the following field indicates that this table is a tenant
    | specific table.
	|
	*/
    'tenant_field_identifier' => 'tenant_id',
];
