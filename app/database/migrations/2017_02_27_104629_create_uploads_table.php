<?php

use Illuminate\Database\Migrations\Migration;
use Webpatser\Uuid\Uuid;

class CreateUploadsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploads', function ($table) {

            $table->engine = 'InnoDB';

            $table->bigIncrements('id');
            $table->integer('tenant_id')->unsigned()->nullable()->default(null);
            $table->string('uuid', 36)->nullable()->default(null);
            $table->string('name', 255)->nullable()->default(null);

            // custom fields starts
            $table->string('path', 512)->nullable()->default(null);
            $table->string('tags', 255)->nullable()->default(null);
            $table->string('desc', 512)->nullable()->default(null);
            $table->integer('module_id')->unsigned()->nullable()->default(null);
            $table->integer('element_id')->unsigned()->nullable()->default(null);
            $table->integer('element_uuid')->unsigned()->nullable()->default(null);

            // custom fields ends
            $table->string('is_active', 3)->nullable()->default(null);
            $table->integer('created_by')->unsigned()->nullable()->default(null);
            $table->integer('updated_by')->unsigned()->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
            $table->integer('deleted_by')->unsigned()->nullable()->default(null);
        });

        DB::table('modules')->insert(
            [
                'uuid'           => Uuid::generate(4),
                'name'           => 'uploads',
                'title'          => 'Upload',
                'desc'           => '',
                'parent_id'      => 0,
                'modulegroup_id' => 0,
                'level'          => 0,
                'order'          => 0,
                'color_css'      => 'aqua',
                'icon_css'       => 'fa fa-plus',
                'route'          => 'uploads.index',
                'is_active'      => 'Yes',
                'created_at'     => now(),
                'created_by'     => '1',
                'updated_at'     => now(),
                'updated_by'     => '1'
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // drop the module table
        Schema::dropIfExists('uploads');
        // remove the module entry from modules table
        DB::table('modules')->where('name', 'uploads')->delete();
    }

}