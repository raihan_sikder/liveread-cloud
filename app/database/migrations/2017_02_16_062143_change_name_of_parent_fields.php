<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeNameOfParentFields extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `spyr42_modules` CHANGE `parent_module_id` `parent_id` INT(10) UNSIGNED NULL DEFAULT NULL;");
        DB::statement("ALTER TABLE `spyr42_modulegroups` CHANGE `parent_modulegroup_id` `parent_id` INT(10) UNSIGNED NULL DEFAULT NULL;");
        DB::statement("ALTER TABLE `spyr42_permissioncategories` CHANGE `parent_permissioncategory_id` `parent_id` INT(10) UNSIGNED NULL DEFAULT NULL;");
        DB::statement("ALTER TABLE `spyr42_permissions` CHANGE `parent_permission_id` `parent_id` INT(10) UNSIGNED NULL DEFAULT NULL;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}
