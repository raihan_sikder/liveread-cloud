<?php

/*
|--------------------------------------------------------------------------
| Spyr Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the spyr framework.
|
*/
$modules = Module::names(); // fetch all module names
$modulegroups = Modulegroup::names();
$ns = ''; // common namespace for spyr controllers
Route::get('/', ['as' => 'home', 'uses' => $ns . 'DashboardController@show']);


/*
 *
 * Resources / RESTful routes.
 * ***************************************************************************
 * Resources automatically generates index, create, store, show, edit, update,
 * destroy routes. Based on the modules table all these routes are created.
 * In addition to above we also need a 'restore' as we have soft delete
 * enabled for our solution.
 *
 * prefix    :
 * filter    : before [auth] - only authenticated users can access these routes
 *        : before [resource.route.permission.check] - checks permission using Sentry.
 *****************************************************************************/


Route::group(['before' => ['auth']], function () use ($modules, $modulegroups, $ns) {
    # default routes for all modules
    foreach ($modules as $module) {
        $Controller = $ns . ucfirst($module) . "Controller";
        Route::get($module . "/{id}/restore", ['as' => $module . '.restore', 'uses' => $Controller . '@restore']);
        Route::get($module . "/grid", ['as' => $module . '.grid', 'uses' => $Controller . '@grid']); // returns datatable json
        /**
         * Revisions are shown in a separate page with an aim to improve performance where revisions/change log of an element
         * is only shown when a user requests for it.
         */
        Route::get($module . "/{id}/revisions", ['as' => $module . '.revisions', 'uses' => $Controller . '@revisions']);
        Route::resource($module, $Controller); // for some reason this resource route needs be placed at the bottom otherwise it does work.
    }
    # default routes for all modulegroups
    foreach ($modulegroups as $modulegroup) {
        Route::get("$modulegroup", ['as' => "$modulegroup.index", 'uses' => 'ModulegroupsController@modulegroupIndex']);
    }

    /**
     * Generate download request of a file.
     * Files are stored in a physical file system. To hide the urls from the user following URL generates a download
     * request that initiates download of the file matching the uuid.
     */
    Route::get('download/{uuid}', ['as' => 'get.download', 'uses' => 'UploadsController@download']);
});

# croppic image cropper
Route::post('croppic-crop', ['as' => 'croppic-crop', 'uses' => 'UploadsController@croppicCrop']);


//Route::get('create-superuser', ['as' => 'create-superuser', 'uses' => 'AuthController@createSuperUser']);
//Route::get('seed-module-permissions', ['as' => 'seed-module-permissions', 'uses' => 'AddlpermissionsController@seedModulePermissions']);

